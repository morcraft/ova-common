const _ = require('lodash');

module.exports = {
    changeSlideState: function(args){
        //console.log('args', args);
        if(!_.isObject(args)){
            throw('Some arguments are expected.');
        }
        if(typeof args.slideId === 'undefined' || args.slideId.length < 1){
            throw('A slide id is expected');
        }
        
        var $slide = $('.slide[data-slide="' + args.slideId + '"]');
        //console.log('$slide', $slide);
        var data = $slide.data('presentation');
        //console.log('callinggg data', data);
        if(_.isObject(data)){
            data.enabled = !!args.state;
        }
    },
    enableSlide: function(slideId){
        return this.changeSlideState({
            slideId: slideId,
            state: true
        });
    },
    disableSlide: function(slideId){
        return this.changeSlideState({
            slideId: slideId,
            state: false
        });
    },
    setGroupsState: function(args){
        var data = this.checkArgsForGroupProcess(args);
        //console.log('data', data);
        data.$groups.each(function(e){
            var index = e + 1;
            $(this).data('enabled', args.state[index] !== false);
        });

        return true;
    },
    setIndividualGroupState: function(args){
        var data = this.checkArgsForGroupProcess(args);

        if(isNaN(args.groupIndex)){
            throw('A group index is expected');
        }

        if(!data.$groups.eq(args.groupIndex).length){
            console.warn('Group with index ' + args.groupIndex + ' doesn\'t exist in slide with ID ' + args.slideId);
            return true;
        }

        return data.$groups.eq(args.groupIndex).data('enabled', !!args.state.enabled);
    },
    checkArgsForGroupProcess: function(args){
        if(!_.isObject(args)){
            throw('Some arguments are expected.');
        }
        if(typeof args.slideId === 'undefined' || args.slideId.length < 1){
            throw('A slide id is expected');
        }

        if(!_.isObject(args.state)){
            throw('A state with indexes is expected.');
        }

        var $slide = $('.slide[data-slide="' + args.slideId + '"]');
        if(!$slide.length){
            throw('Slide with ID ' + args.slideId + ' wasn\'t found.');
        }

        var $groups = $slide.children('.group');
        if(!$groups.length){
            throw('Slide with ID ' + args.slideId + ' has no groups to manage.');
        }

        return {
            $slide: $slide,
            $groups: $groups
        }
    }
}