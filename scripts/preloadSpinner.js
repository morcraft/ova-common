module.exports = {
    messageElement: document.getElementById('ova-load-spinner-text'),
    messageIcon: document.getElementById('ova-load-spinner-icon'),
    spinner: document.getElementById('lds-dual-ring'),
    element: document.getElementById('ova-load-spinner'),
    updateMessage:function(message){
        message = typeof message !== 'string' ? 'Cargando...' : message;
        this.messageElement.innerHTML = message;
        return this;
    },
    hideSpinner: function(){
        this.spinner.style.display = 'none';
        return this;
    },
    updateIcon: function(icon){
        iconString = typeof icon !== 'string' ? '' : icon;
        this.messageIcon.innerHTML = iconString;
        return this;
    },
    hide: function(){
        var self = this;
        self.element.style.opacity = 0;
        setTimeout(function(){
            self.element.style.display = 'none'
        }, 550);
    }
}