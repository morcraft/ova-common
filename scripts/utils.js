/*  
    ¡No modificar!
    Eventos y funciones útiles que no requieren un módulo individual.
*/
const presentation = require('presentation');
const isMobile = require('is-mobile');
const ovaConcepts = require('ova-concepts');
const _ = require('lodash');
const screenfull = require('screenfull')

module.exports = function (ovaConfig) {

    if (isMobile()) {
        $('html').addClass('is-mobile');
    }

    $('body').on('click', '.click-me', function () {
        $(this).addClass('clicked')
    })

    $('body').on('mouseenter', '.hover-me', function () {
        $(this).addClass('hovered')
    })

    $('body').on('click', '[data-switch-to-slide]', function () {
        if ($(this).hasClass('disabled')) {
            return false;
        }
        var target = $(this).attr('data-switch-to-slide');
        presentation.switchToSlide({
            slide: $('.slide[data-slide="' + target + '"]').eq(0)
        })
    });

    $('.concepts-modal .close-button').on('click', function () {
        $('.concepts-modal').hide()
    });

    $('.secondary-info .glossary').on('click', function () {
        ovaConcepts.showEarnedConceptsModal()
    });

    _.forEach(ovaConfig.slidesConfig.disabledSlides, function (v, k) {
        $('.slide[data-slide="' + v + '"]').data('presentation').enabled = false;
    });

    $('.ova-content-header .fullscreen').on('click', function(){
        if (screenfull.enabled){
            screenfull.toggle();
        }
    })
}